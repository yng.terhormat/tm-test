<div class="row">
	<div class="col-12 col-s-12">
		
		<form id="patient" name="patient">
			<label class="w3-text-blue">Name:</label>
      <input class="step-input w3-border" type="text" id="txtname" name="txtname" value="Patients">
			<br><br>

			<label class="w3-text-blue">Alamat:</label>
			<input class="step-input w3-border" type="text" id="txtaddress" name="txtaddress" value="Jl. Alamat">
			<br><br>
			
			<label class="w3-text-blue">Birthday:</label>
			<input class="step-input w3-border" type="text" id="txtbirthday" name="txtbirthday" value='{{ date("Y-m-d 00:00:00") }}'>
			<br><br>

			<input type="button" id="save" name="save" value="Add New">
		</form>
		<br>

		<div id="pagination" class="w3-center">
		{!! $pagination !!}
		</div>
		<div class="scrollable"> 
			<table id="tbl-patient" class="table-adminer" style="width: 100%">
				<thead>
					<tr>
						<th>Medical Records Id</th>
						<th>Name</th>
						<th>Address</th>
						<th>Birthday</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $key => $r)
					<tr>
						<td>{{ $r->rmid }}</td>
						<td>{{ $r->name }}</td>
						<td>{{ $r->address }}</td>
						<td>{{ date("d-m-Y", strtotime($r->birthday)) }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<!--<div id="test-pag" class="test-pag">
		</div>
		<br>-->
		
	</div>
</div>

<script type="text/javascript">
	$("#save").click(function () {
		var prm = 'keyword=addnew&' + $('#patient').serialize(); //console.log(host + '-' + prm);
		_post('patient-d', prm, 'views');
		//resviews(prm);
	})
</script>