<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ModPatient;

class ConPatient extends Controller {
    
	public function master(Request $request) {
		$prm = array();
		$prm['data'] = $this->view(); //ModPatient::orderby('patientid', 'desc')->paginate(2); //dd($prm['data']);
		$prm['pagination'] = view('pagination', $prm);
		$prm['child'] = view('childs.patient', $prm); //"Master Patients";
		return view('index-', $prm);
	}
	public function send_(Request $request) {
		$keyword = $request->input('keyword'); //echo "[$keyword]";
		switch ($keyword) {
			case 'addnew':
				//$this->addpatient($request);
				$prm = array();
				$prm['data'] = $this->view(); //ModPatient::orderby('patientid', 'desc')->paginate(2); //dd($prm['data']);
				$prm['pagination'] = view('pagination', $prm)->render();
				return $prm;
			break;
			
			default:
				# code...
				break;
		}/**/
	}
	private function addpatient($req) { //keyword=addnew&txtname=asdf&txtaddress=asdf&txtbirthday=asdf
		$arr = array();
		$arr['name'] = $req->input('txtname');
		$arr['birthday'] = $req->input('txtbirthday');
		$arr['address'] = $req->input('txtaddress'); //var_dump($arr);
		$id = ModPatient::p_new($arr, 1); //$id = 1;
		$rmid = substr(date("Y"), -2)."-".substr("000000".$id, -6); //echo $rmid;
		$res = ModPatient::p_update(['id' => $id, 'rmid' => $rmid]); //echo "[$id]";//."-".$res;
	
	return $res;
	}
	private function view() {
		$patient = ModPatient::orderby('patientid', 'desc')->paginate(2);
		return $patient->withPath('patient');
	}
}
