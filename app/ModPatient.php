<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModPatient extends Model {
	protected $table = "patient";
	protected $primaryKey = "patientid";

  public static function p_new($prm, $i=1) {
    $p_new = new static;
    foreach ($prm as $key => $val) $p_new->$key = $val;
    $res = $p_new->save();
    if ($i) $res = $p_new->patientid;
  return $res;
  }
  public static function p_update($prm) { //var_dump($prm);
    $p_update = static::find($prm['id']);
    foreach ($prm as $key => $val) {
      if ($key!="id") $p_update->$key = $val;
    }
  return $p_update->save();
  }
}
