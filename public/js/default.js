var host = '{{ URL::to('/') }}';

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(document).ready(function() {
		//$("#myModal").modal('show');
	});

	function _json(str) {
		var obj = JSON.parse(str);
		return obj;
	}
	function isJson(text) {
    if (typeof text=="object") return true;
    else return false;
	}
	function _post(path, prm, callback) {
		$('body').addClass('waiting');
		isajax = true; //index.blade.php
		$.ajax({
			//url: host + '/' + path, //'mypage.html',
			url: path,
			type: "POST",
			data: prm,
			success: function(res) {
				//console.log(res);
				if (callback!='') _parse(callback, res);
				else {
					addalert("child", "Success " + res); //alert('success');
					isajax = false;
				}
			},
			error: function(err) {
				$('body').removeClass('waiting');
				addalert("child", "Error " + err); //alert(err);
				isajax = false;
			}
		});
	}

	function _parse(id, res) {
		var sp = id.split("|");
		$('body').removeClass('waiting');
		switch(sp[0]) {
			case "views": 	//index-.blade.php
				//_reload();
				resviews(res);
			break;
			case "addbook": 	//index-.blade.php
				resaddbook(sp[1], res); //alert(res);
			break;
			default:
				//alert(res);
				addalert("child", "Respon unregistered");
		}
	isajax = false;
	}
	function resviews(res) { //var json_ = '{"data":{"current_page":1,"data":[{"patientid":11,"rmid":"19-000011","name":"Patients","address":"Jl. Alamat","birthday":"2019-02-26 00:00:00","created_at":"2019-02-26 02:46:18","updated_at":"2019-02-26 02:46:18","deleted_at":null},{"patientid":10,"rmid":"19-000010","name":"Patients","address":"Jl. Alamat","birthday":"2019-02-26 00:00:00","created_at":"2019-02-26 02:46:14","updated_at":"2019-02-26 02:46:14","deleted_at":null}],"first_page_url":"http:\/\/127.0.0.1:8000\/patient-d?page=1","from":1,"last_page":6,"last_page_url":"http:\/\/127.0.0.1:8000\/patient-d?page=6","next_page_url":"http:\/\/127.0.0.1:8000\/patient-d?page=2","path":"http:\/\/127.0.0.1:8000\/patient-d","per_page":2,"prev_page_url":null,"to":2,"total":11}}';
		var json = res; //_json(res); //console.log(json.data.data.length); //pagination(json);
		var tr = '';
		tr += '<thead><tr>' +
					'	<th>Medical Records Id</th>' +
					'	<th>Name</th>' +
					' <th>Address</th>' +
					'	<th>Birthday</th>' +
					'</tr></thead><tbody>';
		var i;
		for (i in json.data.data) {
			//console.log(json.data.data[i].patientid);
			tr += '<tr>';
			tr += '<td>' + json.data.data[i].rmid + '</td>';
			tr += '<td>' + json.data.data[i].name + '</td>';
			tr += '<td>' + json.data.data[i].address + '</td>';
			tr += '<td>' + json.data.data[i].birthday + '</td>';
			tr += '</tr>';
		}
		tr += '</tbody>';

		$('#tbl-patient').html(tr); //tbl-patient
		$('#pagination').html(json.data.pagination);
	}
	/*function pagination(json) {
		var res = '';
		res += '<div class="w3-bar">';
		res += '<a href="' + json.data.first_page_url  + '" class="w3-button">First</a>';
		res += '<a href="javascript:void(0)" class="w3-button">«</a>';

		var limit = 2;
		if ((json.data.from + limit) < json.data.last_page) max = json.data.from + limit;
		else max = json.data.last_page;
		
		for (i = 1; i <= max; i++) { 
		  if (i==json.data.current_page) res += '<a href="javascript:void(0)" class="w3-button w3-blue">' + i + '</a>';
		  else res += '<a href="' + json.data.path + '?page=' + i + '" class="w3-button">' + i + '</a>';
		}

		res += '<a href="'+ json.data.path +'?page=' + (json.data.current_page+1) + '" class="w3-button">»</a>';
		res += '<a href="'+ json.data.last_page_url + '" class="w3-button">Last</a>';
		console.log(res);
		$('#pagination').html(res);
	}*/

	function addalert(id, msg) {
		/*var messages =	'<div id="errordiv" [class] style="margin-top: 20px" role="alert">' +
										'	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
										'		<i class="fa fa-fw fa-info-circle"></i> <span id="msgerror">' + msg + '</span> '+
										'</div>';
		if ($('#errordiv').length > 0) $('#msgerror').html(msg);
		else if (id=='child') {
			messages = messages.replace('[class]', 'class="alert alert-info shadowed"');
			$('#' + id).prepend(messages);
		} else {
			messages = messages.replace('[class]', 'class="alert alert-warning alert-dismissible fade in shadowed"');
			$('#' + id).append(messages); //class="alert alert-warning alert-dismissible fade in shadowed"
		}*/ /*$('#' + id).append(messages);*/
	}

  function _reload() {
    setTimeout(function() {
      location.reload();
    }, 1000);/**/
  }